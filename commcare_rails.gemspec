$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "commcare_rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "commcare_rails"
  s.version     = CommcareRails::VERSION
  s.authors     = ["Guillaume Deflaux"]
  s.email       = ["deflaux.guillaume@gmail.com"]
  s.homepage    = "http://github.com/gdeflaux/commcare_rails"
  s.summary     = "Active Record classes to manipulate and store data from CommCareHQ"
  s.description = "Active Record classes to manipulate and store data from CommCareHQ"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.0"
  s.add_dependency "commcare_api"
  s.add_dependency "json"

  s.add_development_dependency "sqlite3"
end
