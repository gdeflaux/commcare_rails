class CreateCommcareRailsCases < ActiveRecord::Migration
  def change
    create_table :commcare_rails_cases do |t|
      t.string :case_id
      t.boolean :closed
      t.datetime :date_closed
      t.datetime :date_modified
      t.string :domain
      t.datetime :server_date_modified
      t.datetime :server_date_opened
      t.text :properties
      t.string :case_name
      t.string :case_type
      t.string :owner_id
      t.string :external_id
      t.datetime :date_opened
      t.string :resource_uri
      t.text :indices
      t.string :xform_ids

      t.timestamps
    end
  end
end
