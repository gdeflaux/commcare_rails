module CommcareRails
  class Engine < ::Rails::Engine
    isolate_namespace CommcareRails
  end
end
