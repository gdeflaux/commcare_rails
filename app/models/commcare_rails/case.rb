require 'commcare_api'

module CommcareRails
  class Case < ActiveRecord::Base
    
    validates_uniqueness_of :case_id
    
    def update_from_hq(username, password)
      #c_hq = self.get_case(username, password, domain, case_id)
    end
    
    def open?
      !closed
    end
    
    # Returns a single CCRAILS::Case
    def self.get_case(username, password, domain, cid, options = {})
      ccc = CommcareApi::CommcareConnector.new(username, password)
      response = ccc.get_case(domain, cid, options)
      
      # if response.code = 200
      j = JSON.parse(response.body)
      self.create_object(j)
    end
    
    # Returns a array of CCRAILS::Case
    def self.get_cases(username, password, domain, options = {})
      ccc = CommcareApi::CommcareConnector.new(username, password)
      response = ccc.get_cases(domain, options)
      
      # if response.code = 200
      cases = []
      j = JSON.parse(response.body)
      j["objects"].each do |c_json|
         cases << self.create_object(c_json)
      end
      cases
    end
    
    def self.get_all_new_from_hq(username, password, domain, options = {})
      # returns and array of cases that are not yet stored.
    end
    
    def self.update_all_form_hq(username, password, domain, options = {})
      # gets the cases from hq and updates their stored instance
      # if a case from hq does have a local instance it is discarded
    end
    
    protected
    
    def self.create_object(j)
      c = Case.new
      c.case_id = j["case_id"]
      c.closed = j["closed"]
      c.date_closed = DateTime.parse j["date_closed"] if !j["date_closed"].nil?
      c.date_modified = DateTime.parse j["date_modified"] if !j["date_modified"].nil?
      c.domain = j["domain"]
      c.indices = j["indices"].to_s
      c.case_name = j["properties"]["case_name"]
      c.case_type = j["properties"]["case_type"]
      c.date_opened = DateTime.parse j["date_opened"] if !j["date_opened"].nil?
      c.properties = j["properties"].to_s
      c.owner_id = j["properties"]["owner_id"]
      c.external_id = j["properties"]["external_id"]
      c.resource_uri = j["resource_uri"]
      c.server_date_modified = j["server_date_modified"]
      c.server_date_opened = j["server_date_opened"]
      c.xform_ids = j["xform_ids"].join(",")
      c
    end

  end
end
